﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basiccounterlib
{
    public class Counterclass
    {
        private int compteur;

        public Counterclass(int compteur)
        {
            this.compteur = compteur;
        }

        public void Incrementation()
        {
            compteur = compteur + 1;
        }

        public void Decrementation()
        {
            compteur = compteur - 1;
        }

        public void Remiseazero()
        {
            compteur = 0;
        }

        public int getCounter()
        {
            return compteur;
        }

        public static string ReferenceEquals()
        {
            throw new NotImplementedException();
        }
    }
}