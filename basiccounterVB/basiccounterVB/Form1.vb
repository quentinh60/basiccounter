﻿Imports basiccounterlib

Public Class Form1

    Public compteur As New Counterclass(0)

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label1.Text = String.Format("{0}", compteur.getCounter())
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Increment.Click
        compteur.Incrementation()
        Label1.Text = String.Format("{0}", compteur.getCounter())
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Decrement.Click
        compteur.Decrementation()
        Label1.Text = String.Format("{0}", compteur.getCounter())
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Remiseazero.Click
        compteur.Remiseazero()
        Label1.Text = String.Format("{0}", compteur.getCounter())
    End Sub
End Class
