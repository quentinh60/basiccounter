﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Increment = New System.Windows.Forms.Button()
        Me.Decrement = New System.Windows.Forms.Button()
        Me.Remiseazero = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Increment
        '
        Me.Increment.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Increment.Location = New System.Drawing.Point(471, 130)
        Me.Increment.Name = "Increment"
        Me.Increment.Size = New System.Drawing.Size(136, 57)
        Me.Increment.TabIndex = 0
        Me.Increment.Text = "+"
        Me.Increment.UseVisualStyleBackColor = True
        '
        'Decrement
        '
        Me.Decrement.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Decrement.Location = New System.Drawing.Point(111, 130)
        Me.Decrement.Name = "Decrement"
        Me.Decrement.Size = New System.Drawing.Size(139, 57)
        Me.Decrement.TabIndex = 1
        Me.Decrement.Text = "-"
        Me.Decrement.UseVisualStyleBackColor = True
        '
        'Remiseazero
        '
        Me.Remiseazero.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Remiseazero.Location = New System.Drawing.Point(284, 227)
        Me.Remiseazero.Name = "Remiseazero"
        Me.Remiseazero.Size = New System.Drawing.Size(150, 63)
        Me.Remiseazero.TabIndex = 2
        Me.Remiseazero.Text = "RAZ"
        Me.Remiseazero.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(340, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Label1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 356)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Remiseazero)
        Me.Controls.Add(Me.Decrement)
        Me.Controls.Add(Me.Increment)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Increment As Button
    Friend WithEvents Decrement As Button
    Friend WithEvents Remiseazero As Button
    Friend WithEvents Label1 As Label
End Class
