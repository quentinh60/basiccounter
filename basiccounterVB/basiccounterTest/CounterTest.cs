﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using basiccounterlib;

namespace basiccounterTest
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void TestIncrementation()
        {
            Counterclass compteur = new Counterclass(0);
            compteur.Incrementation();
            Assert.AreEqual(1, compteur.getCounter());
            compteur.Incrementation();
            Assert.AreEqual(2, compteur.getCounter());

        }

        [TestMethod]
        public void TestDecrementatio()
        {
            Counterclass compteur = new Counterclass(2);
            compteur.Decrementation();
            Assert.AreEqual(1, compteur.getCounter());
            compteur.Decrementation();
            Assert.AreEqual(0, compteur.getCounter());
        }

        [TestMethod]
        public void TestRemiseazero()
        {
            Counterclass compteur = new Counterclass(4);
            compteur.Remiseazero();
            Assert.AreEqual(0, compteur.getCounter());
        }
    }
}
